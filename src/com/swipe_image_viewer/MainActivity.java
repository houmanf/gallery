package com.swipe_image_viewer;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

public class MainActivity extends Activity {
	
	public enum MediaMode {PHOTO, VIDEO}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	public void startPhotoMode(View v) {
		startGallery(MediaMode.PHOTO);
	}
	
	public void startVideoMode(View v) {
		startGallery(MediaMode.VIDEO);
	}
	
	private void startGallery(MediaMode mode) {
		Intent intent = new Intent(this, GalleryActivity.class);
		startActivity(intent);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
}
