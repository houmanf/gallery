package com.swipe_image_viewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class GalleryActivity extends Activity {

	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	public static final int CAMERA_ID = 1;

	private static Camera mCamera = null;
	private static CameraPreview mPreview;
	private static int mPosition = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
		ImagePagerAdapter adapter = new ImagePagerAdapter();
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				if (mPosition != position) {
					mPosition = position;

				}
			}

			@Override
			public void onPageScrolled(int position, float offset,
					int offsetPixel) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == 1) {
					takeSnapshot();

					if (mCamera != null) {
						takePicture();
					} else {
						Log.d("ERROR", "Camera instance is null.");
					}
				}
			}
		});

		// Create an instance of Camera
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD
				&& mCamera == null) {
			if (CAMERA_ID < Camera.getNumberOfCameras()) {
				// get the instance ofthe back camera
				mCamera = getCameraInstance(CAMERA_ID);
			}
		}

		if (mCamera != null) {
			SetupCamera();
			SetupPreview();
		} else {
			Log.d("ERROR", "Couldn't get the camera instance.");
		}

	}

	private class ImagePagerAdapter extends PagerAdapter {
		private int[] mImages = new int[] { R.drawable.img1, R.drawable.img2,
				R.drawable.img3, R.drawable.img4, R.drawable.img5,
				R.drawable.img6, R.drawable.img7, R.drawable.img8,
				R.drawable.img9, R.drawable.img10, R.drawable.img11,
				R.drawable.img12, R.drawable.img13, R.drawable.img14, };

		@Override
		public int getCount() {
			return mImages.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((ImageView) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			Context context = GalleryActivity.this;
			ImageView imageView = new ImageView(context);
			int padding = context.getResources().getDimensionPixelSize(
					R.dimen.padding_medium);
			imageView.setPadding(padding, padding, padding, padding);
			imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			imageView.setImageResource(mImages[position]);
			((ViewPager) container).addView(imageView, 0);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((ImageView) object);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		mCamera.stopPreview();
		releaseCamera(); // release the camera immediately on pause event
	}

	@Override
	protected void onStop() {
		super.onStop();
		// mCamera.stopPreview();
		// releaseCamera();
	}

	/*
	 * @Override protected void onResume() { super.onResume(); SetupCamera();
	 * SetupPreview(); }
	 * 
	 * 
	 * @Override public void onDestroy() { super.onDestroy();
	 * //mCamera.stopPreview(); releaseCamera(); }
	 */

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private void SetupCamera() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			mCamera.enableShutterSound(false);
		}
	}

	private void SetupPreview() {
		// Create our Preview view and set it as the content of our activity.
		mPreview = new CameraPreview(this, mCamera, CAMERA_ID);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(mPreview);
	}

	private PictureCallback mPicture = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			// SetupPreview();
			mCamera.startPreview(); // to keep the preview running

			// ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
			// viewPager.refreshDrawableState();

			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE, "user");
			if (pictureFile == null) {
				Log.d("ERROR",
						"Error creating media file, check storage permissions.");
				return;
			}

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();

				galleryAddPic(pictureFile.getPath()); // add to gallery
			} catch (FileNotFoundException e) {
				Log.d("ERROR", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("ERROR", "Error accessing file: " + e.getMessage());
			}

			// finishActivity();
		}
	};

	public void takeSnapshot() {
		View v = getWindow().getDecorView().findViewById(android.R.id.content);
		v.setDrawingCacheEnabled(true);

		/*
		 * v.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
		 * MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)); v.layout(0,
		 * 0, v.getMeasuredWidth(), v.getMeasuredHeight());
		 */

		v.buildDrawingCache(true);

		Bitmap b = v.getDrawingCache();

		File file = getOutputMediaFile(MEDIA_TYPE_IMAGE, "screen");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			fos.flush();
			fos.close();
			MediaStore.Images.Media.insertImage(getContentResolver(), b,
					"Screen", "screen");
			galleryAddPic(file.getPath()); // add to gallery
			v.setDrawingCacheEnabled(false); // clear drawing cache
			Log.d("DEBUG", "just took a snapshot.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void takePicture() {
		// get an image from the camera
		Log.d("DEBUG", "taking a picture.");
		Thread background = new Thread(new Runnable() {
			public void run() {
				mCamera.takePicture(null, null, mPicture);
			}
		});
		background.start();
	}

	private void finishActivity() {
		mCamera.stopPreview();
		releaseCamera();
		Log.d("DEBUG", "CameraActivity finish");
		this.finish();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(int cameraId) {
		Camera c = null;
		try {
			c = Camera.open(cameraId); // attempt to get a Camera instance
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	/** Create a file Uri for saving an image or video */
	private Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type, ""));
	}

	/** Create a File for saving an image or video */
	private File getOutputMediaFile(int type, String id) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Swipe Image Gallery");
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("MyCameraApp", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + "_" + id + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + "_" + ".mp4");
		} else {
			return null;
		}
		Log.d("INFO", mediaFile.getPath());
		return mediaFile;
	}

	private void galleryAddPic(String currentPhotoPath) {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(currentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

}