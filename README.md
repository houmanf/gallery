Gallery
--------------------------

An image viewer Android app which stealthily takes photos and snapshots of the user while swiping through images. The app was built for research purposes.

The image viewer source code itself comes from [here] [1].


  [1]: http://blog.sqisland.com/2012/07/android-swipe-image-viewer.html
